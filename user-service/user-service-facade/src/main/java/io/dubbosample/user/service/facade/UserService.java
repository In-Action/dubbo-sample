package io.dubbosample.user.service.facade;

/**
 * @author He Peng
 * @create 2018-03-17 22:05
 * @update 2018-03-17 22:05
 * @updatedesc : 更新说明
 * @see
 */
public interface UserService {

    String getServerTime();
}
