package io.dubbosample.user.service.core;

import io.dubbosample.user.service.facade.UserService;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author He Peng
 * @create 2018-03-17 22:06
 * @update 2018-03-17 22:06
 * @updatedesc : 更新说明
 * @see
 */

//@Service("userService")
public class UserServiceImpl implements UserService {

    @Override
    public String getServerTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }
}
