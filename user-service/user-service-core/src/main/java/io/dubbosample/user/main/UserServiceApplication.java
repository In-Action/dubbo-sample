package io.dubbosample.user.main;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author He Peng
 * @create 2018-03-17 22:17
 * @update 2018-03-17 22:17
 * @updatedesc : 更新说明
 * @see
 */
public class UserServiceApplication {

    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext applicationContext =
                    new ClassPathXmlApplicationContext("classpath*:spring/applicationContext-root.xml");
        applicationContext.start();
        System.in.read();
    }
}
