import io.dubbosample.user.service.facade.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author He Peng
 * @create 2018-03-17 22:26
 * @update 2018-03-17 22:26
 * @updatedesc : 更新说明
 * @see
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:dubbo/dubbo-consumer.xml")
public class DubboRpcServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void userServiceTest() throws Exception {
        String serverTime = this.userService.getServerTime();
        System.out.println("result ==> " + serverTime);
    }
}
